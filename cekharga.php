<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cek Harga</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<script type="text/javascript">
		
		$(document).ready(function() {

			// Function executes on change of first select option field.
			$("#countryasal").change(function() {
			var select = $("#countryasal option:selected").val();

				cityasal(select);
			
			});

			$("#countrydest").change(function() {
			var select = $("#countrydest option:selected").val();

				citydest(select);
			
			});
			// Function To List out Cities in Second Select tags
			function cityasal(arr) {

					$.ajax({
					    url: "getcity.php?id="+arr,
					    type: 'GET',
					    success: function(res) {
					        console.log(res);
					        $("#cityasal").empty(); //To reset cities
							$("#cityasal").append("<option>--Select--</option>");
							var parsed_data = JSON.parse(res); 
							$(parsed_data).each(function(i) { //to list cities
								$("#cityasal").append('<option value="' + parsed_data[i].city_id + '">' + parsed_data[i].city_name + '</option>')
							});
					    }
					});
			}

			function citydest(arr) {

					$.ajax({
					    url: "getcity.php?id="+arr,
					    type: 'GET',
					    success: function(res) {
					        console.log(res);
					        $("#citydest").empty(); //To reset cities
							$("#citydest").append("<option>--Select--</option>");
							var parsed_data = JSON.parse(res); 
							$(parsed_data).each(function(i) { //to list cities
								$("#citydest").append('<option value="' + parsed_data[i].city_id + '">' + parsed_data[i].city_name + '</option>')
							});
					    }
					});
			}
			
		});

	</script>
</head>
<body>
<?php

require 'CurlHelper.php';

$urlprov = "https://api.rajaongkir.com/starter/province";
$provinsi = new CurlHelper($urlprov, "GET");
$respro = $provinsi->send();
$prodata = $respro['data']['rajaongkir']['results'];

?>

<h1>Cek Harga</h1>
	<br>
	<form action="" method="POST">
		Pilih Provinsi asal:<br>
		<select name="provasal" id="countryasal">
			<?php 
				for ($i = 0; $i < count($prodata); $i++) {
					echo '<option value="'.$prodata[$i]['province_id'].'">'.$prodata[$i]['province'].'</option>';
				}
			 ?>
		</select>
		<br>
		Pilih kota asal:
		<select id="cityasal" name="cityasal">
		</select>
		<br>

		Pilih Provinsi destinasi:<br>
		<select name="provdest" id="countrydest">
			<?php 
				for ($j = 0; $j < count($prodata); $j++) {
					echo '<option value="'.$prodata[$j]['province_id'].'">'.$prodata[$j]['province'].'</option>';
				}
			 ?>
		</select>
		<br>
		Pilih kota destinasi:
		<select id="citydest" name="citydest">
		</select>
		<br>
		Kurir :
		<br>
		<select name="kurir">
			<option value="jne">JNE</option>
			<option value="pos">POS Indonesia</option>
			<option value="tiki">TIKI</option>
		</select>
		<br>
		Berat (Kg):
		<input type="number" name="berat">
		<hr>
		<input type="submit" name="submitall">
	</form>

<?php


if (isset($_POST['submitall'])) {
	$urlcost = "https://api.rajaongkir.com/starter/cost";
	$data = "origin=".$_POST['cityasal']."&destination=".$_POST['citydest']."&weight=".$_POST['berat']."&courier=".$_POST['kurir'];
	$cost = new CurlHelper($urlcost, "POST",$data);
	$rescost = $cost->send();
	$datacost = isset($rescost['data']['rajaongkir']['results'][0]['costs'])?$rescost['data']['rajaongkir']['results'][0]['costs']:0;
}
?>

Hasil Harga :
<br>
<table border="1">
	<tr>
		<td>Jenis Service</td>
		<td>Deskripsi</td>
		<td>Harga</td>
		<td>Estimasi (Hari)</td>
	</tr>
	<tbody>
	<?php 
 		if ($datacost==0) {
 			echo "tidak ada";
 		}else{
 			for ($i = 0; $i < count($datacost); $i++) {
 			echo '<tr>';
 			echo '<td>';
 			echo $datacost[$i]['service'];
 			echo '</td>';
 			echo '<td>';
 			echo $datacost[$i]['description'];
 			echo '</td>';
 			echo '<td>';
 			echo $datacost[$i]['cost'][0]['value'];
 			echo '</td>';
 			echo '<td>';
 			echo $datacost[$i]['cost'][0]['etd'];
 			echo '</td>';
 			echo '</tr>';
		}
 		}
 ?>
 </tbody>
</table>

	
</body>
</html>