<?php 

/**
 * summary
 */
class CurlHelper
{
    /**
     * summary
     */
    private $url;
    private $method;
    private $data;
    private $api = "007629ecb670c53b02eaad3de328e4e9";

    public function __construct($url, $method, $data = "")
    {
        $this->url = $url;
        $this->method = $method;
        $this->data = $data;
    }

    function send(){

    	$curl = curl_init();

    	if ($this->method=="GET") {
    		$arrsend = array(
			  CURLOPT_URL => $this->url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "key: 007629ecb670c53b02eaad3de328e4e9"
			  )
			);
    	}else{
    		$arrsend = array(
			  CURLOPT_URL => $this->url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  // CURLOPT_POSTFIELDS => "origin=501&destination=114&weight=1700&courier=jne",
			  CURLOPT_POSTFIELDS => $this->data,
			  CURLOPT_HTTPHEADER => array(
			    "content-type: application/x-www-form-urlencoded",
			    "key: 007629ecb670c53b02eaad3de328e4e9"
			  )
			);
    	}

    	curl_setopt_array($curl, $arrsend);

    	$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		// var_dump(json_decode($response,true));
		if ($err) {
		  return array(
		  			"status" => 403,
		  			"data" => "cURL Error #:" . $err
		  		);
		} else {
		  return array(
		  			"status" => 200,
		  			"data" => json_decode($response,true)
		  		);
		}
    }
}

 ?>